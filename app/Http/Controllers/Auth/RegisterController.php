<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Events\UserRegisterEvent;
use Illuminate\Http\Request;
use App\User;


class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users,email|email',
        ]);
    
        $data_request = $request->all();
        $user = User::create($data_request);
        
        $data['user'] = $user;

        event(new UserRegisterEvent($user));

        return response()->json([
            'response_code' => '00',
            'response_message' => 'User baru berhasi; didaftarkan, silahkan cek email untuk meilhat kode otp',
            'data' => $data
        ], 200);
    
    }
}
