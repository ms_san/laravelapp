<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class Otp extends Model
{
    use UsesUuid;

    protected $guarded = [];
    public $table = "otp_codes";

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
