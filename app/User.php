<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Traits\UsesUuid;
use App\Otp;
use Carbon\Carbon;


class User extends Authenticatable implements JWTSubject
{
    use Notifiable, UsesUuid;

    public function get_user_id()
    {
        $role = \App\Role::where('name' , 'user')->first();
        return $role->id;
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($model){
            $model->role_id = $model->get_user_id();
        });

        static::created(function ($model) {
            $model->generate_top_code();
        });
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','photo',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function otp()
    {
    	return $this->hasOne(Otp::class);
    }

    public function isAdmin()
    {
        if ($this->role_id === $this->get_user_id()) {
            return false;
        }    
        return true;
    }

    public function generate_top_code()
    {
        do {
            $random = mt_rand(100000,999999);
            $check = Otp::where('otp', $random)->first();
        } while ($check);
        
        $now = Carbon::now();

        $otp_code = Otp::updateOrCreate(
            ['user_id' => $this->id],
            ['otp' => $random, 'valid_until' => $now->addMinutes(5)]
        );
    }
}
