<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Blog;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Blog::class, function (Faker $faker) {
    return [
        'title' => $faker->sentences,
        'description' => $faker->paragraph,
        'image' => $faker->image('public/photos/blog',640,480, null, false),
    ];
});
