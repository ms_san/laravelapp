<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Campaign;
use Faker\Generator as Faker;

$factory->define(Campaign::class, function (Faker $faker) {
    return [
        'title' => $faker->sentences,
        'description' => $faker->paragraph,
        'image' => $faker->image('public/photos/campaign',640,480, null, false),
        'address' =>$faker->address,
        'required' => $faker->numberBetween(1000000,5000000),
        'collected' => $faker->numberBetween(100000,2000000),
    ];
});
