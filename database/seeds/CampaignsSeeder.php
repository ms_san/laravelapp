<?php

use App\Campaign;
use Illuminate\Database\Seeder;


class CampaignsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Campaign::class, 20)->create();
    }
}
